#[allow(unused_imports)]
#[macro_use]
extern crate approx;
extern crate my_internet_ip;

#[allow(dead_code)]
fn init_logging() {
    use std::sync::Once;
    static INIT: Once = Once::new();
    fn init_log4rs() {
        log4rs::init_file("config/log4rs.yml", Default::default()).unwrap();
    }
    INIT.call_once(init_log4rs);
}

pub mod application {
    const SERVER: &str = "127.0.0.1:8001";
    const CLIENT_BASE_ADDR: &str = "127.0.0.1";
    const FIXED_TIME_STEP: f64 = 1.0 / 60.0;

    use bevy::{
        core::FixedTimestep,
        input::mouse::{MouseButton, MouseMotion, MouseWheel},
        prelude::*,
    };

    pub mod networking;
    use networking::{Client, NetworkEvent, Server};
    mod player;

    use rand::Rng;

    enum AppMode {
        Client,
        Server,
    }

    enum ServerIpMode {
        // For use in local testing and development
        Auto,
        // For use when accessing client over the internet.
        UserInput,
    }
    struct AppConfig {
        mode: AppMode,
        server_ip_mode: ServerIpMode,
    }

    pub fn lib_main() {
        let mut args = std::env::args();
        args.next();
        let first_arg = args.next().unwrap();

        let mut config: AppConfig = AppConfig {
            mode: AppMode::Client,
            server_ip_mode: ServerIpMode::Auto,
        };

        if first_arg == "server" {
            config.mode = AppMode::Server;
        } else if first_arg == "client" {
            config.mode = AppMode::Client;
        } else {
            println!(
                "First argument was neither server or client, it was {:?}",
                first_arg
            )
        }

        let second_arg = args.next().unwrap();
        if second_arg == "auto" {
            config.server_ip_mode = ServerIpMode::Auto;
        } else if second_arg == "userinput" {
            config.server_ip_mode = ServerIpMode::UserInput;
        } else {
            println!(
                "Second argument was neither auto or userinput, it was {:?}",
                second_arg
            )
        }

        let ip: ::std::net::IpAddr = match my_internet_ip::get() {
            Ok(ip) => ip,
            Err(e) => panic!("Could not get IP: {:?}", e),
        };

        println!("Got IP {}", ip);

        match config.mode {
            AppMode::Server => make_server_app(
                &mut App::build(),
                make_server(&format!("{}:8001", ip)),
                config,
            )
            .run(),
            AppMode::Client => client_main(config),
        }
    }

    /// ********************************************** Application builder section
    fn make_server(server_ip: &str) -> Server {
        use networking::Socket;
        let server = Server {
            socket: Socket::bind("0.0.0.0:8001").unwrap(),
            connected_clients: Vec::new(),
        };
        println!("Socket is supposedly publically available on {}", server_ip);
        return server;
    }

    fn make_server_app(app: &mut AppBuilder, server: Server, config: AppConfig) -> &mut AppBuilder {
        // Server app
        let mut senders = networking::Senders::new();
        networking::register_protocols(&mut senders, app);
        player::register_protocols(&mut senders, app);

        // networking::init_events(app);
        app.insert_resource(Msaa { samples: 4 })
            .insert_resource(server)
            .insert_resource(config)
            .insert_resource(senders)
            //TODO: Separate message resource initialization into a bundle managed by networking::
            .add_plugins(DefaultPlugins)
            .add_startup_system(setup.system())
            .add_system_set(
                SystemSet::new()
                    .with_run_criteria(FixedTimestep::step(FIXED_TIME_STEP))
                    .with_system(bevy::input::system::exit_on_esc_system.system())
                    .with_system(networking::server_process_network_events.system())
                    .with_system(player::server_spawn_player.system())
                    .with_system(player::update_player.system())
                    .with_system(networking::BeamAttack::despawn_system.system())
                    .with_system(player::send_player_state_to_client.system()),
            )
    }

    fn make_client_app(app: &mut AppBuilder, client: Client, config: AppConfig) -> &mut AppBuilder {
        // networking::init_events(app);
        let mut senders = networking::Senders::new();
        networking::register_protocols(&mut senders, app);
        player::register_protocols(&mut senders, app);

        app.insert_resource(Msaa { samples: 4 })
            .insert_resource(client)
            .insert_resource(config)
            .insert_resource(senders)
            .add_plugins(DefaultPlugins)
            // WARNING: SHARED WITH SEVER!
            .add_startup_system(setup.system())
            .add_system_set(
                SystemSet::new()
                    .with_run_criteria(FixedTimestep::step(FIXED_TIME_STEP))
                    .with_system(bevy::input::system::exit_on_esc_system.system())
                    .with_system(networking::client_process_network_events.system())
                    .with_system(client_try_connect.system())
                    .with_system(client_process_input_events.system())
                    .with_system(player::client_update_player.system()),
            )
    }

    fn make_client(binding_address: &str) -> Client {
        use networking::{ClientState, Socket};
        Client {
            socket: Socket::bind(binding_address).unwrap(),
            id: 0,
            state: ClientState::NotConnected,
            // TODO: figure out callback IP in a dynamic way.
            ip_address: String::from(binding_address),
            frames_since_connection_attempt: 0,
        }
    }

    fn client_main(config: AppConfig) {
        let mut rng = rand::thread_rng();
        let port = rng.gen_range(8001..8500);
        let client_sock_ip = format!("{}:{}", CLIENT_BASE_ADDR, port);
        // TODO: use make_client
        let client = make_client(&client_sock_ip);

        make_client_app(&mut App::build(), client, config) // Client app
            .run();
    }

    // TODO: In the future, spawn these dynamically and sync them like we sync the players
    fn setup(
        mut commands: Commands,
        mut meshes: ResMut<Assets<Mesh>>,
        mut materials: ResMut<Assets<StandardMaterial>>,
    ) {
        commands.spawn().insert_bundle(PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Plane { size: 1000.0 })),
            material: materials.add(Color::rgb(0.2, 0.9, 0.2).into()),
            transform: Transform::from_translation(Vec3::new(0.0, 0.0, 0.0)),
            ..Default::default()
        });
        commands.spawn().insert_bundle(LightBundle {
            transform: Transform::from_translation(Vec3::new(4.0, 5.0, 10.0)),
            ..Default::default()
        });
    }

    fn client_try_connect(
        mut connection: ResMut<networking::Client>,
        mut connect_success_event: ResMut<NetworkEvent<networking::RandomAccessAcknowledge>>,
    ) {
        use networking::{ClientState, MessageType, PlayerRandomAccess};
        match (*connection).state {
            ClientState::NotConnected => {
                info!("Attemting connect to {}", SERVER);
                networking::send_message(
                    MessageType::PlayerRandomAccess,
                    &PlayerRandomAccess {
                        source_ip: connection.ip_address.clone(),
                    },
                    &mut connection.socket,
                    SERVER,
                );
                connection.state = ClientState::Connecting;
                connection.frames_since_connection_attempt = 0;
            }
            ClientState::Connecting => {
                for event in &mut *connect_success_event {
                    connection.state = ClientState::Connected;
                    (*connection).id = event.player_uid;
                    info!("Successfully connected to {}", SERVER);
                }
                connection.frames_since_connection_attempt += 1;
                if connection.frames_since_connection_attempt
                    > (1.0 / FIXED_TIME_STEP).floor() as u16
                {
                    connection.state = ClientState::NotConnected;
                }
            }
            ClientState::Connected => {
                /*
                Do nothing, we're already connected. In the future, might do some fault recovery?
                */
            }
        }
    }

    //TODO: Looking around should only happen when you right click.
    //      Left click is for targetting spells etc
    fn client_process_input_events(
        mut mouse_motion_events: EventReader<MouseMotion>,
        mut mouse_wheel_events: EventReader<MouseWheel>,
        mouse_buttons: Res<Input<MouseButton>>,
        keyboard_input: Res<Input<KeyCode>>,
        mut connection: ResMut<networking::Client>,
    ) {
        use player::Movement;
        match connection.state {
            networking::ClientState::Connected => {}
            _ => {
                // Escape early if our connection has yet to be acknowledged.
                return;
            }
        }

        let mut movement_message: Movement = Movement::default();
        // TODO: figure out why mouse movement is so choppy
        for event in mouse_motion_events.iter() {
            movement_message.look = event.delta;
        }
        for event in mouse_wheel_events.iter() {
            movement_message.zoom_delta = event.y;
        }
        movement_message.mouse_left = mouse_buttons.pressed(MouseButton::Left);
        movement_message.mouse_right = mouse_buttons.pressed(MouseButton::Right);
        movement_message.mouse_middle = mouse_buttons.pressed(MouseButton::Middle);

        if keyboard_input.pressed(KeyCode::W) {
            movement_message.forward = true;
        }
        if keyboard_input.pressed(KeyCode::S) {
            movement_message.back = true;
        }
        if keyboard_input.pressed(KeyCode::D) {
            movement_message.right = true;
        }
        if keyboard_input.pressed(KeyCode::A) {
            movement_message.left = true;
        }
        if keyboard_input.pressed(KeyCode::Space) {
            movement_message.jump = true;
        }

        movement_message.delta_seconds = FIXED_TIME_STEP as f32; //time.delta_seconds();
                                                                 //println!("sending {:?}", movement_message);
        movement_message.client_uid = (*connection).id;
        networking::send_message(
            networking::MessageType::Movement,
            &movement_message,
            &mut connection.socket,
            SERVER, // TODO: this can clearly be more elegant
        );
    }

    #[cfg(test)]
    mod tests {
        use super::*;
        use crate::init_logging;

        fn make_test_config(mode: AppMode) -> AppConfig {
            AppConfig {
                mode: mode,
                // Probably will almost always be auto in test suites.
                server_ip_mode: ServerIpMode::Auto,
            }
        }

        /// Utility functions
        fn add_test_stage(app: &mut AppBuilder, stage_contents: SystemStage) -> &mut AppBuilder {
            app.add_stage_after(CoreStage::Last, "testing", stage_contents);
            return app;
        }

        use networking::{Client, Server, Socket};

        pub fn make_client(binding_address: &str) -> Client {
            Client {
                socket: Socket::bind(binding_address).unwrap(),
                id: 0,
                state: networking::ClientState::NotConnected,
                ip_address: String::from(binding_address),
                frames_since_connection_attempt: 0,
            }
        }

        /// Networking module integration tests
        /// IPs need to not collide with the networking module, so we use port 5**** and 4****
        /// for server/client respectively.
        #[test]
        fn test_player_connect_sequence_server_side() {
            init_logging();
            enum TestState {
                SendPlayerConnect,
                RecieveServerResponse,
                CheckForPlayerSpawned,
            }

            const CLIENT_IP: &str = "127.0.0.1:50001";
            let client = make_client(CLIENT_IP);
            const SERVER_IP: &str = "127.0.0.1:40001";
            let server = make_server(SERVER_IP);
            let app = &mut App::build();
            app.insert_resource(TestState::SendPlayerConnect);
            app.insert_resource(client);
            let config = make_test_config(AppMode::Server);

            make_server_app(app, server, config);

            let stage = SystemStage::single_threaded()
                .with_system(networking::client_process_network_events.system())
                .with_system(test.system());
            add_test_stage(app, stage);
            use networking::{MessageType, PlayerRandomAccess, RandomAccessAcknowledge};

            // Verify that a player object is spawned after server response.
            fn test(
                mut state: ResMut<TestState>,
                mut connection: ResMut<Server>,
                client_connection: ResMut<Client>,
                mut ack_recieved: ResMut<NetworkEvent<RandomAccessAcknowledge>>,
                mut query: Query<&mut player::Player>,
                mut app_exit_events: EventWriter<bevy::app::AppExit>,
            ) {
                match *state {
                    TestState::SendPlayerConnect => {
                        networking::send_message(
                            MessageType::PlayerRandomAccess,
                            &PlayerRandomAccess {
                                source_ip: client_connection.ip_address.clone(),
                            },
                            &mut connection.socket,
                            SERVER_IP,
                        );
                        *state = TestState::RecieveServerResponse;
                    }
                    TestState::RecieveServerResponse => {
                        for event in &mut *ack_recieved {
                            assert!(event.player_uid != 0);
                            assert!(event.player_uid == client_connection.id);
                            *state = TestState::CheckForPlayerSpawned;
                        }
                    }
                    TestState::CheckForPlayerSpawned => {
                        for player in &mut query.iter_mut() {
                            assert!(player.player_uid != 0);
                            app_exit_events.send(bevy::app::AppExit);
                        }
                    }
                };
            }
        }
    }
}
