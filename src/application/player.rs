use bevy::prelude::*;
pub use laminar::{Packet, Socket, SocketEvent};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

// use serde::ser::{Serialize, SerializeStruct, Serializer};
use crate::application::networking;
use networking::{BeamAttack, Client, NetworkEvent, Server};
use serde_derive::{Deserialize, Serialize};

use super::FIXED_TIME_STEP;

pub fn register_protocols(senders: &mut networking::Senders, app: &mut AppBuilder) -> () {
    use networking::MessageType;
    senders
        .register_protocol::<PlayerWithTransform>(app, MessageType::PlayerWithTransform)
        .register_protocol::<Movement>(app, MessageType::Movement);
}

// TODO: Rename movement to input, or make two separate messages...
#[derive(Serialize, Deserialize, Default, Debug, PartialEq)]
pub struct Movement {
    pub forward: bool,
    pub left: bool,
    pub back: bool,
    pub right: bool,
    pub mouse_left: bool,
    pub mouse_middle: bool,
    pub mouse_right: bool,
    pub jump: bool,
    pub look: Vec2,
    pub zoom_delta: f32,
    pub delta_seconds: f32,
    pub client_uid: u64,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Player {
    pub yaw: f32,
    pub camera_distance: f32,
    pub camera_pitch: f32,
    pub camera_entity: Option<Entity>,
    pub jumping: bool,
    pub jump_time: f32,
    pub player_uid: u64,
    pub player_ip: String,
    pub beam_attack: BeamAttack,
}

impl Default for Player {
    fn default() -> Self {
        Player {
            yaw: 0.,
            camera_distance: 20.,
            camera_pitch: 30.0f32.to_radians(),
            camera_entity: None,
            jumping: false,
            jump_time: 0.0,
            player_uid: 0,
            player_ip: String::from(""),
            beam_attack: BeamAttack::default(),
        }
    }
}

#[derive(Serialize, Deserialize)]
#[serde(remote = "Transform")]
struct TransformDef {
    translation: Vec3,
    rotation: Quat,
    scale: Vec3,
}

#[derive(Serialize, Deserialize, Default)]
pub struct PlayerWithTransform {
    pub player: Player,
    #[serde(with = "TransformDef")]
    pub transform: Transform,
}

fn spawn_player(
    commands: &mut Commands,
    meshes: &mut ResMut<Assets<Mesh>>,
    materials: &mut ResMut<Assets<StandardMaterial>>,
    with_camera: bool,
    player: Player,
    transform: Transform,
) {
    let cube_material_handle = materials.add({
        let cube_material: StandardMaterial = Color::rgb(1.0, 0.3, 1.0).into();
        cube_material
    });

    // Spawn camera and Player. Camera is attached to Player as a member of the Player struct.
    let mut player_to_spawn = player;
    let mut camera_entity: Option<Entity> = Option::None;
    if with_camera {
        let local_camera_entity = commands
            .spawn()
            .insert_bundle(PerspectiveCameraBundle::default())
            .id();
        player_to_spawn.camera_distance = 20.;
        camera_entity = Some(local_camera_entity);
        player_to_spawn.camera_entity = camera_entity;
    }

    let mut player_entity = commands.spawn();
    player_entity
        .insert_bundle(PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Cube { size: 1.0 })),
            material: cube_material_handle.clone(),
            transform: transform,
            ..Default::default()
        })
        .insert(player_to_spawn);

    if with_camera {
        /* This should never be None in the with_camera path */
        player_entity.push_children(&[camera_entity.unwrap()]);
    }
}

fn make_player_uid() -> u64 {
    use std::sync::atomic::{AtomicU64, Ordering};
    static COUNTER: AtomicU64 = AtomicU64::new(1);
    COUNTER.fetch_add(1, Ordering::Relaxed)
}

pub fn server_spawn_player(
    mut commands: Commands,
    mut connection: ResMut<Server>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut player_spawned_event: ResMut<NetworkEvent<networking::PlayerRandomAccess>>,
) {
    for event in &mut *player_spawned_event {
        let player_uid = make_player_uid();
        info!("Spawning player on server with id {}", player_uid);
        connection.connected_clients.push(event.source_ip.clone());

        spawn_player(
            &mut commands,
            &mut meshes,
            &mut materials,
            true,
            Player {
                camera_entity: Option::None,
                camera_distance: 20.,
                player_uid: player_uid,
                player_ip: event.source_ip.clone(),
                ..Default::default()
            },
            Transform::from_translation(Vec3::new(0.0, 0.5, 0.0)),
        );
        let payload = networking::RandomAccessAcknowledge {
            player_uid: player_uid,
        };

        // Confirm the successful spawn to client
        info!(
            "Player spawned, acknowledging connect attempt to IP {}",
            event.source_ip
        );
        networking::send_message(
            networking::MessageType::RandomAccessAcknowledge,
            &payload,
            &mut connection.socket,
            &event.source_ip,
        );
    }
}

pub fn client_update_player(
    mut commands: Commands,
    connection: Res<Client>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut player_transform: ResMut<NetworkEvent<PlayerWithTransform>>,
    mut queries: QuerySet<(Query<(&mut Player, &mut Transform)>, Query<&mut Transform>)>,
) {
    for event in &mut *player_transform {
        let mut cam_positions = Vec::new();
        let mut found = false;
        // Update player entity with matching ID if present, otherwise spawn it.
        // If player entity does not already exist, spawn it.
        for (mut player, mut transform) in &mut queries.q0_mut().iter_mut() {
            if player.player_uid != event.player.player_uid {
                continue;
            }
            if event.player.player_uid == connection.id {
                // Preserve the local camera_entity
                let camera_entity = player.camera_entity.clone();
                *player = event.player.clone();
                player.camera_entity = camera_entity;
                if let Some(camera_entity) = player.camera_entity {
                    let cam_pos =
                        Vec3::new(0., player.camera_pitch.cos(), -player.camera_pitch.sin())
                            .normalize()
                            * player.camera_distance;
                    cam_positions.push((camera_entity, cam_pos))
                }
            } else {
                *player = event.player.clone();
            }
            *transform = event.transform.clone();
            found = true;
        }
        if !found {
            info!(
                "Spawning player with uid {}! on client with uid {}",
                event.player.player_uid, connection.id
            );
            let player_is_owned_by_this_client = connection.id == event.player.player_uid;
            spawn_player(
                &mut commands,
                &mut meshes,
                &mut materials,
                player_is_owned_by_this_client,
                event.player.clone(),
                event.transform,
            );
        }

        for (camera_entity, cam_pos) in cam_positions.iter() {
            if let Ok(mut cam_trans) = queries
                .q1_mut()
                .get_component_mut::<Transform>(*camera_entity)
            {
                cam_trans.translation = *cam_pos;

                let look =
                    Mat4::face_toward(cam_trans.translation, Vec3::ZERO, Vec3::new(0.0, 1.0, 0.0));
                cam_trans.rotation = look.to_scale_rotation_translation().1;
            }
        }
    }
}

// This is the server update function. Probably need to change something here...
pub fn update_player(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    time: Res<Time>,
    mut movement_events: ResMut<NetworkEvent<Movement>>,
    mut queries: QuerySet<(Query<(&mut Player, &mut Transform)>, Query<&mut Transform>)>,
) {
    trace!(
        "Delta T was {:?}, goal is {:?}",
        time.delta_seconds(),
        FIXED_TIME_STEP
    );
    for event in &mut *movement_events {
        let mut movement = Vec2::ZERO;
        // Copy event data into local variables
        if event.forward {
            movement.y += 1.;
        }
        if event.back {
            movement.y -= 1.;
        }
        if event.right {
            movement.x += 1.;
        }
        if event.left {
            movement.x -= 1.;
        }
        let jump = event.jump;
        let look = event.look;
        let zoom_delta = event.zoom_delta;

        if movement != Vec2::ZERO {
            movement.normalize();
        }

        let move_speed = 20.0;
        let zoom_sensitivity = 10.0;
        let look_sensitivity = 1.0;
        let delta_seconds = FIXED_TIME_STEP as f32;
        movement *= delta_seconds * move_speed;
        let mut cam_positions = Vec::new();
        let current_time = time.seconds_since_startup() as f32;

        for (mut player, mut transform) in &mut queries.q0_mut().iter_mut() {
            if player.player_uid != event.client_uid {
                continue;
            }

            // TODO: Refactor camera so that it zooms in at pitches above 90 degrees just enough to not
            // go under the ground.
            if true {}

            let time_in_flight: f32 = current_time - player.jump_time;
            let mut height: f32 = 0.;

            if player.jumping {
                if time_in_flight > PLAYER_JUMP_DURATION {
                    player.jumping = false;
                } else {
                    height = player_jump_height(time_in_flight);
                }
            }
            transform.translation.y = height + 0.5; // TODO: hardcoded offset for player size

            if !event.mouse_left {
                player.yaw += look.x * delta_seconds * look_sensitivity;
                player.camera_pitch -= look.y * delta_seconds * look_sensitivity;
                player.camera_distance -= zoom_delta * delta_seconds * zoom_sensitivity;
                // Clamp camera pitch and distance to reasonable intervals
                player.camera_pitch = player
                    .camera_pitch
                    .max(1f32.to_radians())
                    .min(90f32.to_radians());
                player.camera_distance = player.camera_distance.max(5.).min(30.);

                // If LMB is pressed, player is casting, cannot move
                let fwd = transform.local_z();
                let right = Vec3::cross(fwd, Vec3::Y);

                let fwd = fwd * movement.y;
                let right = right * movement.x;

                if jump && !player.jumping {
                    player.jumping = true;
                    player.jump_time = current_time;
                }

                transform.translation += Vec3::from(fwd + right);
                transform.rotation = Quat::from_rotation_y(-player.yaw);

                player.beam_attack.spawn_if_casting(
                    &mut commands,
                    transform.translation,
                    transform.rotation,
                    &mut meshes,
                    &mut materials,
                );
            } else {
                player.beam_attack.channel(delta_seconds);
            }

            if let Some(camera_entity) = player.camera_entity {
                let cam_pos = Vec3::new(0., player.camera_pitch.cos(), -player.camera_pitch.sin())
                    .normalize()
                    * player.camera_distance;
                cam_positions.push((camera_entity, cam_pos))
            }
        }

        for (camera_entity, cam_pos) in cam_positions.iter() {
            if let Ok(mut cam_trans) = queries
                .q1_mut()
                .get_component_mut::<Transform>(*camera_entity)
            {
                cam_trans.translation = *cam_pos;

                let look =
                    Mat4::face_toward(cam_trans.translation, Vec3::ZERO, Vec3::new(0.0, 1.0, 0.0));
                cam_trans.rotation = look.to_scale_rotation_translation().1;
            }
        }
    }
}

pub fn send_player_state_to_client(
    mut connection: ResMut<Server>,
    query: Query<(&Player, &Transform)>,
) {
    // Inform client of the updated state
    let client_list = connection.connected_clients.clone();
    for (player, transform) in query.iter() {
        let player_w_transform = PlayerWithTransform {
            player: (*player).clone(),
            transform: (*transform).clone(),
        };
        for address in client_list.iter() {
            networking::send_message(
                networking::MessageType::PlayerWithTransform,
                &player_w_transform,
                &mut connection.socket,
                &address,
            )
        }
    }
}

const PLAYER_JUMP_DURATION: f32 = 0.8;
const PLAYER_JUMP_DISTANCE: f32 = 3.;
const PLAYER_JUMP_SHAPE_CONSTANT: f32 = 1.0;
const PLAYER_JUMP_HEIGHT: f32 =
    PLAYER_JUMP_DISTANCE * PLAYER_JUMP_DISTANCE / 4.0 * PLAYER_JUMP_SHAPE_CONSTANT;
const PLAYER_JUMP_SPEED: f32 = PLAYER_JUMP_DISTANCE / PLAYER_JUMP_DURATION;
/// Second order polynomial with a maximum at t = jump duration/2
/// and zero at t = 0 and t = PLAYER_JUMP_DURATION
fn player_jump_height(time_in_flight: f32) -> f32 {
    return PLAYER_JUMP_HEIGHT
        - PLAYER_JUMP_SHAPE_CONSTANT
            * (PLAYER_JUMP_SPEED * time_in_flight - PLAYER_JUMP_DISTANCE / 2.0)
            * (PLAYER_JUMP_SPEED * time_in_flight - PLAYER_JUMP_DISTANCE / 2.0);
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::init_logging;
    /// Basic tests for jump curve function.
    #[test]
    fn player_jump_height_0_at_boundaries() {
        init_logging();
        assert_relative_eq!(player_jump_height(0.0), 0.0, epsilon = f32::EPSILON);
        assert_relative_eq!(
            player_jump_height(PLAYER_JUMP_DURATION),
            0.0,
            epsilon = f32::EPSILON
        );
    }
    #[test]
    fn player_jump_height_height_in_middle() {
        init_logging();
        assert_relative_eq!(
            player_jump_height(PLAYER_JUMP_DURATION / 2.0),
            PLAYER_JUMP_HEIGHT,
            epsilon = f32::EPSILON
        );
    }
    #[test]
    fn player_jump_height_check_intermediate() {
        init_logging();
        assert_relative_eq!(
            player_jump_height(PLAYER_JUMP_DURATION / 4.0),
            3.0 / 4.0 * PLAYER_JUMP_HEIGHT,
            epsilon = f32::EPSILON
        );
    }
}
