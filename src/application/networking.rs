// TODO: these IPs will one day not be defaulted in this way... maybe.
use crate::application::CLIENT_BASE_ADDR;
use crate::application::SERVER;
use bincode::{deserialize, serialize};
pub use laminar::{Packet, Socket, SocketEvent};

use serde::ser::Serialize;
use serde_bytes;
use serde_derive::{Deserialize, Serialize};
use std::{marker::PhantomData, time::Instant};

use crossbeam_channel::{unbounded, Receiver, Sender};

// This module handles game engine data and owns Systems.
use bevy::prelude::*;

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

pub enum ClientState {
    NotConnected,
    Connecting,
    Connected,
}
pub struct Client {
    pub socket: Socket,
    pub id: u64,
    pub state: ClientState,
    pub ip_address: String,
    pub frames_since_connection_attempt: u16,
}

impl Default for Client {
    fn default() -> Self {
        Client {
            socket: Socket::bind(format!("{}:8001", CLIENT_BASE_ADDR)).unwrap(),
            id: 0,
            state: ClientState::NotConnected,
            ip_address: format!("{}:8001", CLIENT_BASE_ADDR),
            frames_since_connection_attempt: 0,
        }
    }
}

pub struct Server {
    pub socket: Socket,
    pub connected_clients: Vec<String>,
    // One socket for all clients
    // hashmap<playerSessions>?
}
impl Server {
    pub const ID: u64 = u64::MAX;
}

impl Default for Server {
    fn default() -> Self {
        Server {
            socket: Socket::bind(SERVER).unwrap(),
            connected_clients: Vec::new(),
        }
    }
}

pub struct Senders([Option<Sender<Vec<u8>>>; MessageType::NTypes as usize]);
impl Senders {
    pub fn new() -> Senders {
        const INIT: Option<Sender<Vec<u8>>> = None;
        Senders([INIT; MessageType::NTypes as usize])
    }

    pub fn register_protocol<T>(
        &mut self,
        app: &mut AppBuilder,
        message_type: MessageType,
    ) -> &mut Self
    where
        T: serde::de::DeserializeOwned,
        T: Default,
        T: 'static,
    {
        // TODO: How could we make this use a bounded channel instead?
        // Boundeds are incredibly risky as too many messages can cause a hang (if recieve blocks,
        // we never go to the system stages that consume the messages!)
        let (sender, receiver) = unbounded::<Vec<u8>>();
        self[message_type] = Some(sender);
        let net_receiver = NetworkEvent::<T> {
            receiver: receiver,
            phantom: PhantomData,
        };
        app.insert_resource(net_receiver);

        self
    }
}
use std::ops::{Index, IndexMut};
impl Index<MessageType> for Senders {
    type Output = Option<Sender<Vec<u8>>>;

    fn index(&self, message_type: MessageType) -> &Self::Output {
        &self.0[message_type as usize]
    }
}
impl IndexMut<MessageType> for Senders {
    fn index_mut(&mut self, message_type: MessageType) -> &mut Self::Output {
        &mut self.0[message_type as usize]
    }
}

pub fn register_protocols(senders: &mut Senders, app: &mut AppBuilder) -> () {
    senders
        .register_protocol::<PlayerRandomAccess>(app, MessageType::PlayerRandomAccess)
        .register_protocol::<RandomAccessAcknowledge>(app, MessageType::RandomAccessAcknowledge)
        .register_protocol::<BeamAttack>(app, MessageType::BeamAttack);
}

pub struct NetworkEvent<T>
where
    T: serde::de::DeserializeOwned,
    T: Default,
{
    receiver: Receiver<Vec<u8>>,
    phantom: std::marker::PhantomData<fn() -> T>,
}

impl<T> NetworkEvent<T>
where
    T: serde::de::DeserializeOwned,
    T: Default,
{
    pub fn receive(&self) -> Result<T, crossbeam_channel::TryRecvError> {
        match self.receiver.try_recv() {
            Ok(payload) => {
                return Ok(deserialize_and_handle_error::<T>(payload));
            }
            Err(error) => {
                return Err(error);
            }
        }
    }
}

impl<T> Iterator for NetworkEvent<T>
where
    T: serde::de::DeserializeOwned,
    T: Default,
{
    type Item = T;
    fn next(&mut self) -> Option<T> {
        match self.receive() {
            Ok(payload) => {
                return Some(payload);
            }
            Err(_) => {
                return None;
            }
        }
    }
}

pub struct PlayerSpawned {
    pub id: u64,
    pub ip_address: String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Copy)]
#[repr(u8)]
pub enum MessageType {
    None,
    String,
    Movement,
    PlayerRandomAccess,
    RandomAccessAcknowledge,
    PlayerWithTransform,
    BeamAttack,
    NTypes,
}

impl Default for MessageType {
    fn default() -> Self {
        MessageType::None
    }
}

#[derive(Serialize, Deserialize, Debug, Default)]
struct Message {
    message_type: MessageType,
    uncompressed_size: u32,
    #[serde(with = "serde_bytes")]
    payload: Vec<u8>,
}

#[derive(Serialize, Deserialize, Default, Debug, PartialEq)]
pub struct PlayerRandomAccess {
    // IP adress that can be used to contact the player again.
    pub source_ip: String,
}

#[derive(Serialize, Deserialize, Default, Debug, PartialEq)]
pub struct RandomAccessAcknowledge {
    // IP adress that can be used to contact the player again.
    pub player_uid: u64,
}

#[allow(dead_code)]
#[derive(Serialize, Deserialize)]
#[serde(remote = "Transform")]
struct TransformDef {
    translation: Vec3,
    rotation: Quat,
    scale: Vec3,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct BeamAttack {
    damage: f32,
    length: f32,
    radius: f32,
    casting: bool,
}

impl Default for BeamAttack {
    fn default() -> Self {
        BeamAttack {
            damage: 0.,
            length: 50.0,
            radius: 3.0,
            casting: false,
        }
    }
}
impl BeamAttack {
    pub fn channel(&mut self, delta_seconds: f32) {
        self.casting = true;
        self.damage += 20.0 * delta_seconds;
        self.length += 5.0 * delta_seconds;
        self.radius += 0.3 * delta_seconds;
    }
    pub fn spawn_if_casting(
        &mut self,
        commands: &mut Commands,
        position: Vec3,
        facing: Quat,
        meshes: &mut ResMut<Assets<Mesh>>,
        materials: &mut ResMut<Assets<StandardMaterial>>,
    ) {
        if self.casting {
            debug!("Casting beam!");
            // spawn beam attack
            let beam_material_handle = materials.add({
                let beam_material: StandardMaterial = Color::rgb(1.0, 0., 0.0).into();
                beam_material
            });

            let mut transform = Transform {
                translation: position,
                rotation: facing,
                ..Default::default()
            };
            transform.translation += transform.local_z() * self.length / 2.0;

            commands
                .spawn()
                .insert_bundle(PbrBundle {
                    mesh: meshes.add(Mesh::from(shape::Box::new(
                        2.0 * self.radius,
                        2.0 * self.radius,
                        self.length,
                    ))),
                    material: beam_material_handle.clone(),
                    transform: transform,
                    ..Default::default()
                })
                .insert(BeamData {
                    lived_frames: 0,
                    damage: self.damage,
                    length: self.length,
                    radius: self.radius,
                });

            self.casting = false;
            self.reset();
        }
    }

    pub fn despawn_system(mut commands: Commands, mut query: Query<(Entity, &mut BeamData)>) {
        for (entity, mut beam_timer) in &mut query.iter_mut() {
            if beam_timer.lived_frames < 60 {
                beam_timer.lived_frames += 1;
            } else {
                commands.entity(entity).despawn();
            }
        }
    }

    fn reset(&mut self) {
        *self = BeamAttack::default();
    }
}
pub struct BeamData {
    lived_frames: u32,
    damage: f32,
    length: f32,
    radius: f32,
}

/// Replaces any sequence of up to 255 zeroes in a sequence with a 0 and an 8 bit COUNT of the
/// number of zeroes. Does the replacement in place.
/// Opts out of compression if result is longer than input
fn compress(data: &mut Vec<u8>) {
    let mut curr: usize = 0;
    let mut search: usize;
    let mut new: usize = 0;
    while curr < (data.len()) {
        if data[curr] == 0 {
            // Compress interval
            let mut searching = true;
            search = curr + 1;
            while searching {
                if search < data.len() && data[search] == 0 && (search - curr) < u8::MAX as usize {
                    search += 1;
                } else {
                    searching = false;
                }
            }
            let number_of_zeroes: u8 = (search - curr) as u8;
            if number_of_zeroes == 1 {
                data[new] = 0;
                // Expensive!!
                data.insert(new + 1, 1);
                new += 2;
                curr += 2;
            } else {
                data[new] = 0;
                data[new + 1] = number_of_zeroes;
                new += 2;
                curr = search;
            }
        } else {
            // Increment the new pointer to indicate we keep the vec value. Increment curr to check
            // next value.
            data[new] = data[curr];
            new += 1;
            curr += 1;
        }
    }
    data.resize(new, 0);
}

/// Decompresses a Vec<8> that was compressed using compress(...)
/// TODO: Once I have a logger I want this to log how much compression it does :]
fn decompress(data: Vec<u8>, uncompressed_len: usize) -> Vec<u8> {
    let mut uncompressed: Vec<u8> = vec![0; uncompressed_len];
    let mut uncompressed_index: usize = 0;
    let mut index = 0;
    while (index) < data.len() {
        if data[index] == 0 {
            uncompressed_index += (data[index + 1]) as usize;
            index += 2;
        } else {
            uncompressed[uncompressed_index] = data[index];
            uncompressed_index += 1;
            index += 1;
        }
    }
    return uncompressed;
}

// TODO: Extend with QoS (unreliable, ordered, etc) in the future
/// Serializes payload and sends it to target_ip using socket.
/// Payload will be deserialized when recieved based on the
/// MessageType so MessageType and the type T have to match.
///
/// Arguments:
/// message_type  information used to deserialize Message struct. Has to match payload type
/// payload       message to be sent in the form of a struct.
/// socket        the socket object that the user wishes to send on.
/// target_ip     IP to which the message should be sent.
pub fn send_message<T: Serialize>(
    message_type: MessageType,
    payload: &T,
    socket: &mut Socket,
    target_ip: &str,
) {
    let mut payload = serialize::<T>(&payload).unwrap();
    let uncompressed_size = payload.len();

    debug!(
        "Sending message of type {:?} to receiver {}",
        message_type, target_ip
    );
    trace!("Sending Message: {:?} to reciever {}", payload, target_ip);

    compress(&mut payload);
    let message = Message {
        message_type: message_type,
        uncompressed_size: uncompressed_size as u32,
        payload: payload,
    };

    socket
        .send(Packet::unreliable(
            target_ip.parse().unwrap(),
            serialize::<Message>(&message).unwrap(),
        ))
        .unwrap();
    socket.manual_poll(Instant::now());
}

fn recieve_packets_and_send_events(socket: &mut Socket, senders: &mut Senders) {
    socket.manual_poll(Instant::now());

    while let Some(pkt) = socket.recv() {
        match pkt {
            SocketEvent::Packet(pkt) => {
                match deserialize::<Message>(pkt.payload()) {
                    Err(message) => {
                        error!("Failed to decode message! ErrorKind: {}", message);
                    }
                    Ok(mut message) => {
                        debug!(
                            "Got message from {}, message type {:?}",
                            pkt.addr(),
                            message.message_type
                        );
                        //println!("Decompressing {:?}", message.payload);
                        message.payload =
                            decompress(message.payload, message.uncompressed_size as usize);
                        //println!("Recieved {:?}", message.payload);
                        // TODO: error handle
                        match &senders[message.message_type] {
                            Some(initialized_sender) => {
                                initialized_sender.send(message.payload).unwrap()
                            }
                            None => {
                                error!(
                                    "Tried to send unregistered event! Message type was {:?}",
                                    message.message_type
                                );
                            }
                        }
                    }
                }
            }
            _ => {}
        }
    }
}

fn deserialize_and_handle_error<T>(payload: Vec<u8>) -> T
where
    T: serde::de::DeserializeOwned,
    T: Default,
{
    match deserialize::<T>(&payload) {
        Ok(result) => return result,
        Err(err) => error!(
            "Failed to decode message of type {:?}, bytes were {:?}. Error kind was {}.",
            std::any::type_name::<T>(),
            payload,
            err
        ),
    }
    return T::default();
}

/// This function is a networking system, called by the ECS. For use in the server.
pub fn server_process_network_events(mut connection: ResMut<Server>, mut senders: ResMut<Senders>) {
    recieve_packets_and_send_events(&mut connection.socket, &mut senders);
}

/// This function is a networking system, called by the ECS. For use in the client.
pub fn client_process_network_events(mut connection: ResMut<Client>, mut senders: ResMut<Senders>) {
    recieve_packets_and_send_events(&mut connection.socket, &mut senders);
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::init_logging;

    pub fn make_server(server_ip: &str) -> Server {
        Server {
            socket: Socket::bind(server_ip).unwrap(),
            connected_clients: Vec::new(),
        }
    }

    impl Senders {
        fn register_test_protocol<T>(&mut self, message_type: MessageType) -> NetworkEvent<T>
        where
            T: serde::de::DeserializeOwned,
            T: Default,
            T: 'static,
        {
            let (sender, receiver) = unbounded::<Vec<u8>>();
            self[message_type] = Some(sender);
            NetworkEvent::<T> {
                receiver: receiver,
                phantom: PhantomData,
            }
        }
    }

    pub fn make_client(server_ip: &str) -> Client {
        Client {
            socket: Socket::bind(server_ip).unwrap(),
            id: 0,
            state: ClientState::NotConnected,
            ip_address: String::from(server_ip),
            frames_since_connection_attempt: 0,
        }
    }
    /// Tests in this module need to use different IPs/ports for their sockets,
    /// since they might be run in parallel.
    /// Server ports will start from 20000 and client ports from 30000. We pray.
    #[test]
    fn basic_send_and_recieve_string() {
        init_logging();
        let client_ip = "127.0.0.1:30000";
        let mut client = make_client(client_ip);
        let server_ip = "127.0.0.1:20000";
        let mut server = make_server(server_ip);
        let payload = String::from("I wonder if this message will arrive safely?");

        send_message(MessageType::String, &payload, &mut client.socket, server_ip);

        let mut senders = Senders::new();
        let mut netevent = senders.register_test_protocol::<String>(MessageType::String);
        let mut message_recieved = false;
        let mut i = 0;
        while !message_recieved && i < 100 {
            recieve_packets_and_send_events(&mut server.socket, &mut senders);
            for received_payload in &mut netevent {
                message_recieved = true;
                assert!(payload == received_payload);
            }
            i += 1;
        }
        assert!(
            i < 100,
            "Message never arrived, loop terminated to avoid hang."
        );
    }

    #[test]
    fn basic_send_and_recieve_movement_struct() {
        init_logging();
        use crate::application::player::Movement;
        let client_ip = "127.0.0.1:30001";
        let mut client = make_client(client_ip);
        let server_ip = "127.0.0.1:20001";
        let mut server = make_server(server_ip);
        let payload = Movement::default();

        send_message(
            MessageType::Movement,
            &payload,
            &mut client.socket,
            server_ip,
        );

        let mut senders = Senders::new();
        let mut netevent = senders.register_test_protocol::<Movement>(MessageType::Movement);
        let mut message_recieved = false;
        let mut i = 0;
        while !message_recieved && i < 100 {
            recieve_packets_and_send_events(&mut server.socket, &mut senders);
            for received_payload in &mut netevent {
                message_recieved = true;
                assert!(payload == received_payload);
            }
            i += 1;
        }
        assert!(
            i < 100,
            "Message never arrived, loop terminated to avoid hang."
        );
    }

    #[test]
    fn compression_basic_case() {
        init_logging();
        let mut vec: Vec<u8> = vec![4, 0, 0, 0, 0, 1];
        compress(&mut vec);
        assert_eq!(vec.len(), 4);
        assert_eq!(vec, [4, 0, 4, 1]);
    }

    #[test]
    fn compression_source_ends_with_0() {
        init_logging();
        let mut vec: Vec<u8> = vec![4, 0, 0, 0, 0, 0];
        compress(&mut vec);
        assert_eq!(vec.len(), 3);
        assert_eq!(vec, [4, 0, 5]);
    }

    #[test]
    fn compression_source_begins_with_0() {
        init_logging();
        let mut vec: Vec<u8> = vec![0, 0, 0, 0, 0, 4];
        compress(&mut vec);
        assert_eq!(vec.len(), 3);
        assert_eq!(vec, [0, 5, 4]);
    }

    #[test]
    fn compression_source_has_over_255_contiguous_zeroes() {
        init_logging();
        let mut vec: Vec<u8> = vec![0; 1000];
        vec[452] = 3;
        compress(&mut vec);
        assert_eq!(vec.len(), 11);
        assert_eq!(vec, [0, 255, 0, 197, 3, 0, 255, 0, 255, 0, 37]);
    }

    #[test]
    fn compression_source_has_multiple_holes() {
        init_logging();
        let mut multiple_holes_vec: Vec<u8> = vec![
            0, 0, 0, 0, 0, 0, 160, 65, 146, 10, 6, 63, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
            0, 0, 0, 15, 0, 0, 0, 0, 0, 0, 0, 49, 50, 55, 46, 48, 46, 48, 46, 49, 58, 49, 54, 55,
            55, 51, 0, 0, 0, 0, 0, 0, 0, 63, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            128, 63, 0, 0, 128, 63, 0, 0, 128, 63, 0, 0, 128, 63,
        ];
        compress(&mut multiple_holes_vec);
        let expected_results: Vec<u8> = vec![
            0, 6, 160, 65, 146, 10, 6, 63, 1, 2, 0, 8, 1, 0, 7, 15, 0, 7, 49, 50, 55, 46, 48, 46,
            48, 46, 49, 58, 49, 54, 55, 55, 51, 0, 7, 63, 0, 18, 128, 63, 0, 2, 128, 63, 0, 2, 128,
            63, 0, 2, 128, 63,
        ];
        assert_eq!(expected_results, multiple_holes_vec);
    }

    #[test]
    fn decompression_source_has_multiple_holes() {
        init_logging();
        let multiple_holes_vec: Vec<u8> = vec![
            0, 0, 0, 0, 0, 0, 160, 65, 146, 10, 6, 63, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
            0, 0, 0, 15, 0, 0, 0, 0, 0, 0, 0, 49, 50, 55, 46, 48, 46, 48, 46, 49, 58, 49, 54, 55,
            55, 51, 0, 0, 0, 0, 0, 0, 0, 63, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            128, 63, 0, 0, 128, 63, 0, 0, 128, 63, 0, 0, 128, 63,
        ];
        compress_decompress_test(multiple_holes_vec);
    }

    #[test]
    fn decompression_result_is_longer_than_source() {
        init_logging();
        let mut vec: Vec<u8> = vec![0, 1, 0, 1, 0, 1, 0, 1, 0, 1];
        compress(&mut vec);
        let expected_results = vec![0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1];
        assert_eq!(expected_results, vec);
    }

    fn compress_decompress_test(original: Vec<u8>) {
        init_logging();
        let mut vec: Vec<u8> = original.clone();
        compress(&mut vec);
        vec = decompress(vec, original.len());
        assert_eq!(vec, original);
    }

    #[test]
    fn decompression_basic_case() {
        init_logging();
        let vec: Vec<u8> = vec![4, 0, 0, 0, 0, 1];
        compress_decompress_test(vec);
    }

    #[test]
    fn decompression_source_ends_with_0() {
        init_logging();
        let vec: Vec<u8> = vec![4, 0, 0, 0, 0, 0];
        compress_decompress_test(vec);
    }

    #[test]
    fn decompression_source_begins_with_0() {
        init_logging();
        let vec: Vec<u8> = vec![0, 0, 0, 0, 0, 4];
        compress_decompress_test(vec);
    }

    #[test]
    fn decompression_source_has_over_255_contiguous_zeroes() {
        init_logging();
        let mut vec: Vec<u8> = vec![0; 1000];
        vec[452] = 3;
        compress_decompress_test(vec);
    }

    #[test]
    // TODO: Why is this test empty? What was I thinking? What is missing?
    fn compression_decompression_does_not_break_serde() {}
}
