use log4rs;
use tb_learns_bevy::application;

fn main() {
    log4rs::init_file("config/log4rs.yml", Default::default()).unwrap();
    application::lib_main();
}
