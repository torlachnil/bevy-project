/* One day, figure out a sane way to do testing like this =)
Perhaps using network traces and doing replays.
use std::thread;
use std::time::Duration;
use tb_learns_bevy::application;

#[test]
fn run_client_and_server_expect_no_crash_for_5_seconds() {
    thread::spawn(|| {
        application::server_main();
    });
    thread::sleep(Duration::from_millis(250));
    thread::spawn(|| {
        application::client_main();
    });
    thread::sleep(Duration::from_millis(5000));
}
 */
