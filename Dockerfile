FROM utensils/opengl:stable

ENV CARGO_HOME=/opt/cargo

ENV PATH=$CARGO_HOME/bin:$PATH

RUN apk update
RUN apk add curl
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y  --default-toolchain none

RUN rustup toolchain add stable beta nightly

RUN rustup component add --toolchain nightly rustfmt-preview

RUN rustup component add --toolchain nightly clippy-preview

# RUN apk update && apk add libasound2-dev libudev-dev
RUN apk add build-base cmake git python3 && apk upgrade
# for local testing
COPY Cargo.toml ./
RUN mkdir src && touch src/main.rs && touch txt.txt
RUN rm -rf ~/.cargo
RUN cargo fetch --verbose
# Patch bevy to build on alpine... (with shaderc)
ADD ci_utils ./
RUN cp shader.rs $(find -name shader.rs | grep bevy_render)
RUN cp render_cargo.toml $(find -name Cargo.toml | grep bevy_render)
RUN cp build.rs $(find -name build.rs | grep glsl-to-spirv)
# RUN cargo build

